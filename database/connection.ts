import mongoose from 'mongoose';

export const configureDatabase = async () => {
  mongoose.connect(process.env.DB_URI || '', {})
    .then(() => { console.log('🟢 Conectado a la Base de Datos.'); mongoose.set('debug', true); })
    .catch((error: Error) => { console.error(`🔴 Error al conectarse con la Base de Datos: ${error}.`); });
};