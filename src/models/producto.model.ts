import { model, Schema } from 'mongoose';
import IProducto from '../interfaces/producto.interface';

const ProductoSchema = new Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es obligatoria'],
    },
    precio: {
        type: Number,
        required: [true, 'El precio es obligatorio']
    },
    stock: {
        type: Number,
        required: [true, 'El stock es obligatorio'],
    },
    estado: {
        type: String,
        default: 'nuevo',
        enum: ['nuevo', 'usado']
    },
    ventas: [{
        type: Schema.Types.ObjectId,
        ref: 'Ventas',
    }]

    }, {
      timestamps: { createdAt: true, updatedAt: true }

})


export default model<IProducto>('Productos', ProductoSchema);