import { model, Schema } from 'mongoose';
import IPersona from '../interfaces/persona.interface';

const PersonaSchema = new Schema({

    nombreCompleto: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        required: [true, 'El email es obligatorio']
    },
    contraseña: {
        type: String,
        required: [true, 'La contraseña es obligatorio']
    },
    telefono: {
        type: String,
        required: [true, 'El telefono es obligatorio']
    },
    rol: {
        type: String,
        required: [true, 'El rol es obligatorio'],
        enum: ['admin', 'cliente']
    }
})

export default model<IPersona>('Personas', PersonaSchema);