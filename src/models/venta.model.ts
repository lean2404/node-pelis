import { model, Schema } from 'mongoose';
import IVenta from '../interfaces/venta.interface';

const VentaSchema = new Schema({

    forma_de_pago: {
        type: String,
        default: 'contado',
        enum: ['contado', 'tarjeta']
      },
    precio_total: {
        type: Number,
        default: 0
      },
    estado: {
        type: String,
        default: 'APROBADA',
        enum: ['APROBADA', 'ANULADA']
      },
    persona_id: {
        type: Schema.Types.ObjectId,
        ref: 'Personas',
        required: true
      },
    productos: [{
        type: Schema.Types.ObjectId,
        ref: 'Productos',
      }]

    }, {
      timestamps: { createdAt: true, updatedAt: true }




    
})

export default model<IVenta>('Ventas', VentaSchema);