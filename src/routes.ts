import { Application } from 'express';

// Import routes
import PersonaRoutes from './routes/persona.routes';
import ProductoRoutes from './routes/producto.routes';
import VentaRoutes from './routes/venta.routes';

export const registeredRoutes = async (app:Application) => {
  app.use('/api/usuarios', PersonaRoutes);
  app.use('/api/productos', ProductoRoutes);
  app.use('/api/ventas', VentaRoutes);
};