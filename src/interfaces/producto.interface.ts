import { Document } from 'mongoose';
import { ObjectId } from 'mongodb';

export default interface IProducto extends Document {
    _id: string;
    nombre: string;
    precio: number;
    stock: number;
    estado: string;

    ventas: Array<string>;
    
    createdAt: Date;
    updatedAt: Date;
};