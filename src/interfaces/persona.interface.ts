import { Document } from 'mongoose'

export default interface IPersona extends Document {
  _id: string;
  nombreCompleto: string;
  email: string;
  contraseña: string;
  telefono: string;
  rol: string;
};