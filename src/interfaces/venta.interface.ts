import { Document } from 'mongoose';
import { ObjectId } from 'mongodb';

export default interface IVenta extends Document {
    _id: string;
    forma_de_pago: string;
    precio_total: number;
    estado: string;

    persona_id: ObjectId;
    productos: Array<string>;
    
    createdAt: Date;
    updatedAt: Date;
};