import { Request, Response } from "express";
import Producto from "../models/producto.model";
import IProducto from "../interfaces/producto.interface";

// Get all resources
export const index = async (req: Request, res: Response) => {

    try {

        let producto = await Producto.find()
            // .populate('materias', 'id nombre duracion');
            .populate({ path: 'ventas', select: ['id', 'forma_de_pago', 'precio_total', 'estado'] }); 

        res.json(producto);
        
    } catch (error) {
        res.status(500).send('Algo salió mal' + error);
    }
};

// Get one resource
export const show = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let producto = await Producto.findById(id)
            .populate({ path: 'ventas', select: ['id', 'forma_de_pago', 'precio_total', 'estado'] }); 

        if (!producto)
            res.status(404).send(`No se encontró el producto con id: ${id}`);
        else
            res.json(producto);
    } catch (error) {
        res.status(500).send('Algo salió mal.' + error);
        
        
    }
};

// Create a new resource
export const store = async (req: Request, res: Response) => {
    try {
        const { ...data } = req.body;

        const producto: IProducto = new Producto({
            nombre: data.nombre,
            precio: data.precio,
            stock: data.stock,
            estado: data.estado,
        });

        await producto.save();

        res.status(200).json(producto);
    } catch (error) {
        console.log(error);
        res.status(500).send('Algo salió mal.' + error);
    }
};

// Edit a resource
export const update = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    const { ...data } = req.body;
    try {
        let producto = await Producto.findById(id);

        if (!producto)
            return res.status(404).send(`No se encontró el producto con id: ${id}`);
        
        if (data.nombre) producto.nombre = data.nombre;
        if (data.precio) producto.precio = data.precio;
        if (data.stock) producto.stock = data.stock;
        if (data.estado) producto.estado = data.estado;

        await producto.save();
        
        res.status(200).json(producto);
    } catch (error) {
        res.status(500).send('Algo salió mal.' + error);
    }
};

// Delete a resource
export const destroy = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let producto = await Producto.findByIdAndDelete(id);
        console.log(producto);
        if (!producto)
            res.status(404).send(`No se encontró el producto con id: ${id}`);
        else
            res.status(200).json(producto);
    } catch (error) {
        res.status(500).send('Algo salió mal.' + error);
    }
};