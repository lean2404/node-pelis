import { Request, Response } from "express";
import Venta from "../models/venta.model";
import Producto from "../models/producto.model";
import IVenta from "../interfaces/venta.interface";


// Get all resources
export const index = async (req: Request, res: Response) => {
    // agregar filtros

    try {
        let venta = await Venta.find()
            // .populate('persona_id', 'email nombreCompleto contraseña');
            .populate({ path: 'persona_id', select: ['id', 'nombreCompleto', 'email'] })
            // .populate('materias', 'id nombre duracion');
            .populate({ path: 'productos', select: ['id', 'nombre', 'precio', 'estado'] }); 

        res.json(venta);
    } catch (error) {
        res.status(500).send('Algo salió mal' + error);
    }
};

// Get one resource
export const show = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let venta = await Venta.findById(id)
            // .populate('persona_id', 'email nombreCompleto contraseña');
            .populate({ path: 'persona_id', select: ['id', 'nombreCompleto', 'email'] })
            // .populate('materias', 'id nombre duracion');
            .populate({ path: 'productos', select: ['id', 'nombre', 'precio', 'estado'] }); 

        if (!venta)
            res.status(404).send(`No se encontró la venta con id: ${id}`);
        else
            res.json(venta);
    } catch (error) {
        res.status(500).send('Algo salió mal.' + error);
        
        
    }
};

// Create a new resource
export const store = async (req: Request, res: Response) => {
    try {
        const { ...data } = req.body;

        const venta: IVenta = new Venta({
            forma_de_pago: data.forma_de_pago,
            //precio_total: data.precio_total,
            estado: data.estado,
            persona_id: data.persona_id
        });

        await venta.save();

        res.status(200).json(venta);
    } catch (error) {
        console.log(error);
        res.status(500).send('Algo salió mal.' + error);
    }
};

// Edit a resource
export const update = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    const { ...data } = req.body;
    try {
        let venta = await Venta.findById(id);

        if (!venta)
            return res.status(404).send(`No se encontró la venta con id: ${id}`);
        
        if (data.forma_de_pago) venta.forma_de_pago = data.forma_de_pago;
        //if (data.precio_total) venta.precio_total = data.precio_total;
        if (data.estado) venta.estado = data.estado;
        if (data.persona_id) venta.persona_id = data.persona_id;

        await venta.save();
        
        res.status(200).json(venta);
    } catch (error) {
        res.status(500).send('Algo salió mal.' + error);
    }
};

// Delete a resource
export const destroy = async (req: Request, res: Response) => {
    const id = req?.params?.id;
    try {
        let venta = await Venta.findByIdAndDelete(id);
        console.log(venta);
        if (!venta)
            res.status(404).send(`No se encontró la venta con id: ${id}`);
        else
            res.status(200).json(venta);
    } catch (error) {
        res.status(500).send('Algo salió mal.' + error);
    }
};

export const facturarProducto = async (req: Request, res: Response) => {
    const ventaId = req?.params?.ventaId;
    const productoId = req?.params?.productoId;
    try {
        let venta = await Venta.findById(ventaId);
        let producto = await Producto.findById(productoId);

        if (!venta || !producto)
            res.status(404).send(`No se encontró la venta o el producto indicado.`);
        else if(producto.stock > 0){
            // buscar info acerca de transacciones

            producto.stock--;
            venta.precio_total = venta.precio_total + producto.precio;

            await producto?.ventas.push(venta?.id);
            await venta?.productos.push(producto?.id);
            await producto?.save();
            await venta?.save();
            res.status(201).json(venta);
        }
        else{
            res.status(500).send('No hay stock suficiente de '+producto.nombre);
        }
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

export const reponerProducto = async (req: Request, res: Response) => {
    const ventaId = req?.params?.ventaId;
    const productoId = req?.params?.productoId;

    try {
        let venta = await Venta.findById(ventaId);
        let producto = await Producto.findById(productoId);

        if (!venta || !producto)
            res.status(404).send(`No se encontró la venta o el producto indicado.`);
        else {
            // buscar info acerca de transacciones
            let indiceProducto = Number(venta?.productos.indexOf(productoId));
            let indiceVenta = Number(producto?.ventas.indexOf(ventaId));

            if (indiceProducto !== -1) {
                venta?.productos.splice(indiceProducto,1);
                venta.precio_total = venta.precio_total - producto.precio;
                await venta?.save();
            }

            if (indiceVenta !== -1) {
                producto?.ventas.splice(indiceVenta,1);
                await producto.stock++;
                await producto?.save();
            }

            res.status(201).json(venta);
        }
    } catch (error) {
        res.status(500).send('Algo salió mal.');
    }
};

